minetest.register_on_joinplayer(function(player)

  player:set_pos(hub_manager.get_hub_spawn_point())
  player:get_inventory():set_list("main", {})
  player:get_inventory():set_list("craft", {})

  hub_manager.set_items(player)
  hub_manager.apply_hub_physics(player)

end)



minetest.register_on_player_hpchange(function(player, hp_change, reason)

  reason = reason.type

  -- se non è in arena, disabilito danno da caduta, da affogamento e PvP
  if not arena_lib.is_player_in_arena(player:get_player_name()) and (reason == "fall" or reason == "punch" or reason == "drown") then
    return 0
  end

  return hp_change

end, true)



minetest.register_on_respawnplayer(function(player)

  if arena_lib.is_player_in_arena(player:get_player_name()) then return end

  player:set_pos(hub_manager.get_hub_spawn_point())
  return true

end)
