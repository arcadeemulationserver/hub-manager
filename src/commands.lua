local S = minetest.get_translator("hub_manager")
minetest.unregister_chatcommand("pulverize")



minetest.register_privilege("hub_admin", {
    description = S("It allows you to use /hubadmin")
})



ChatCmdBuilder.new("hubadmin", function(cmd)

    cmd:sub("prefix :name :prefix:text", function(sender, name, prefix)
        local player = minetest.get_player_by_name(name)

        if prefix == "@remove" then prefix = "" end

        if not player then
          hub_manager.print_error(sender, S("[!] @1 is not online", name))
          return end

        local meta = player:get_meta()

        meta:set_string("hub_manager:prefix", prefix)
        minetest.chat_send_player(sender, S("@1's prefix set to \"@2\"", name, prefix))
    end)



    cmd:sub("prefix_color :name :color", function(sender, name, color)
        local player = minetest.get_player_by_name(name)

        if not player then
          hub_manager.print_error(sender, S("[!] @1 is not online", name))
          return end

        local meta = player:get_meta()

        meta:set_string("hub_manager:prefix_color", color)
        minetest.chat_send_player(sender, minetest.colorize(color, S("@1's prefix color set to @2", name, color)))
    end)

end, {
    description = [[
        - prefix <player> <prefix (@remove to remove it)>
        - prefix_color <player> <color (hexadecimal)>
        ]],
    privs = { hub_admin = true }
})
