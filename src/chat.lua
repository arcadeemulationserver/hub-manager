local format_chat_message = minetest.format_chat_message



function minetest.format_chat_message(name, msg)
  if arena_lib.is_player_in_arena(name) then
    return format_chat_message(name, msg)
  else
    return hub_manager.get_prefix(name) .. format_chat_message(name, msg)
  end
end



function hub_manager.print_error(name, msg)
  minetest.chat_send_player(name, minetest.colorize("#e6482e", msg))
end
