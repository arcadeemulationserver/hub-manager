hub_manager = {}



function hub_manager.get_prefix(name)
  local meta = minetest.get_player_by_name(name):get_meta()
  local prefix = meta:get_string("hub_manager:prefix")
  local color = meta:get_string("hub_manager:prefix_color") or "#ffffff"

  if prefix == "" then return "" end
  return minetest.colorize(color, prefix) .. " "
end



function hub_manager.set_items(player)

  local inv = player:get_inventory()
  local hotbar_items = {
    nil,
    nil,
    nil,
    nil,
    nil,
    nil,
    nil,
    "hub_manager:settings"
  }
  local additional_items = hub_manager.get_additional_items()

  -- eventuali oggetti aggiuntivi
  for i = 1, 7 do
    hotbar_items[i] = additional_items[i]
  end

  inv:set_list("main", hotbar_items)
  inv:set_list("craft", {})
end
