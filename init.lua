local version = "0.1.0-dev"
local modpath = minetest.get_modpath("hub_manager")
local srcpath = modpath .. "/src/"

dofile(srcpath .. "/api.lua")
dofile(srcpath .. "/chat.lua")
dofile(srcpath .. "/chatcmdbuilder.lua")
dofile(srcpath .. "/commands.lua")
dofile(srcpath .. "/load_config.lua")
dofile(srcpath .. "/player_manager.lua")
dofile(srcpath .. "/settings.lua")

minetest.log("action", "[HUB_MANAGER] Mod initialised, running version " .. version)
